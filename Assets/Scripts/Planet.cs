﻿using UnityEngine;
using System.Collections;

public class Planet : MonoBehaviour
{
	public Rocket rocket;
	public bool isAsteroid;

	IEnumerator Start () 
	{
		if ( rocket != null )
		{
			rocket.Land(this);

		    yield return new WaitForEndOfFrame();

            ViewManager.Instance.ChangeView(ViewManager.Instance.planetView);
		}

	}


	void OnMouseDown()
	{
		if (Rocket.Instance.landedPlanet != this)
						return;

		if (GetComponent<SlaveMarket> () != null) {
			ViewManager.Instance.ChangeView (ViewManager.Instance.planetView);
		} else if (GetComponent<WildPlanet> () != null) {
			ViewManager.Instance.ChangeView(ViewManager.Instance.wildPlanetView);
		}
	}

}
