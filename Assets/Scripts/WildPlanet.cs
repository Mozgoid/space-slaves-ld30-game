﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class WildPlanet : MonoBehaviour {


	public int price = 300;
	public AlienType alienType;
	//public int population = 10;
	//public int moneyPerAlien = 1;
    public int moneyForRent = 10;
	public bool isYours = false;
	public List<Slave> randomSlaves;

	// Use this for initialization
	void Start () {
		//StartCoroutine (PopulationGrow ());
		StartCoroutine (GenerateRandomSlaves ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	IEnumerator GenerateRandomSlaves()
	{
		while (true) {
			if(randomSlaves == null)
			{
				Debug.Log("generating random aliens on planet " + gameObject.name);
				randomSlaves  = new List<Slave>();
				for (var t = AlienType.FirstAlien; t < AlienType.AliensCount; ++t) {
				    if (t == alienType)
                    {
                        randomSlaves.Add(new Slave() { type = t, count = Random.Range(5, 15) });
				    }
				    else
                    {
                        randomSlaves.Add(new Slave() { type = t, count = Random.Range(0, 3) });
				    }
					
				}
			}

            ViewManager.Instance.wildPlanetView.UpdateContent();
			yield return new WaitForSeconds(15);
		}
	}

	public void Buy()
	{
		if (isYours) {
			Debug.LogWarning ("already yours");
			return;
		}

		if (Player.Instance.Money < price) {
			Debug.LogWarning ("not enough money for buy planet");
			return;
		}


        ViewManager.Instance.wildPlanetView.buyButton.gameObject.SetActive(false);
        ViewManager.Instance.wildPlanetView.huntButton.gameObject.SetActive(false);
		Player.Instance.Money -= price;
		isYours = true;
		StartCoroutine (GetRent());

	
	}

    //IEnumerator PopulationGrow()
    //{
    //    while (true) {
    //        yield return new WaitForSeconds(4);
    //        if(population < 10) 
    //        {
    //            ++population;
    //        }
    //        else
    //        {
    //            population = (int)((float)population * 1.3f);
    //        }
    //        ViewManager.Instance.wildPlanetView.UpdateContent();
    //    }
    //}

	IEnumerator GetRent()
	{
		while (isYours) {
			yield return new WaitForSeconds(4);
		    var money = moneyForRent;
			//var money = population * moneyPerAlien;
			Debug.Log("got rent from your planet " + money.ToString());
			Player.Instance.Money += money;
		}
	}

	public void Hunt()
	{
		if (isYours) {
			Debug.LogWarning ("you trying to slave aliens from your planet");
			return;
		}
        else if (randomSlaves == null)
        {
            Debug.LogError("no aliens for hunt mini game");
            return;
        }

	    var huntGame = new GameObject("huntMiniGame");
	    huntGame.transform.parent = ViewManager.Instance.wildPlanetView.transform;

        Debug.Log("hunting random aliens");
        foreach (var actor in ViewManager.Instance.wildPlanetView.defaultActors)
        {
            var s = randomSlaves.Find(a => a.type == actor.type);
            if (s == null) continue;
            
            while (s.count > 0)
            {
                var movingRight = Random.Range(0, 2) == 0;
                var y = Random.Range(-4f, 4f);
                var x = movingRight ? -9f : 9f;

                var obj = Instantiate(actor.gameObject) as GameObject;
                var minigmeActor = obj.GetComponent<MiniGameActor>();
                minigmeActor.isMovingRight = movingRight;
                obj.transform.parent = huntGame.transform;
                obj.transform.Translate(x,y,0);
                minigmeActor.speed = Random.Range(0.01f, 0.3f);

                s.count--;
            }
        }
        randomSlaves = null;

		
        ViewManager.Instance.wildPlanetView.StartHunt();

	}

    //void SlaveAliens(int count)
    //{
    //    if (population < count) {
    //        Debug.LogWarning ("you trying slave too many slaves");
    //            return;
    //    } 

    //    Debug.Log("hunted " + count.ToString() + " locals");

    //    Player.Instance.AddAliens (alienType, count);
    //    population -= count;
    //}

}
