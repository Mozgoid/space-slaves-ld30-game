﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public delegate void NewQuestGottenDelegate(Quest q);
	static public NewQuestGottenDelegate NewQuestGotten = delegate {};

	
	public delegate void PlayerInventoryChangedDelegate(List<Slave> inventory);
	static public PlayerInventoryChangedDelegate PlayerInventoryChanged = delegate {};



	private List<Rocket> _rockets;
	private List<Planet> _planets;

	public List<Rocket> Rockets
	{
		get {
			if(_rockets == null)
			{
				_rockets = new List<Rocket>();
			}
			return _rockets;
		}
	}


	private static GameManager _instance;
	public static GameManager Instance
	{
		get 
		{
			if(_instance == null)
			{
				var obj = new GameObject("game manager");
				_instance = obj.AddComponent<GameManager>();
			}
			return _instance;
		}
	}

	void Awake()
	{
		if(_instance == null)
		{
			_instance = this;
		}
	}

	void OnDestroy()
	{
		if(_instance == this)
		{
			_instance = null;
		}
	}

	
	// Update is called once per frame
	void Update () {
	
	}




}
