﻿using UnityEngine;
using System.Collections;

public class WildPlanetViewContoller : ViewController {

    public bool hunted;
    public GameObject huntButton;
    public AlienState[] aliens;
    public GameObject AliensOnPlanetStatus;
    public MiniGameActor[] defaultActors;
    public BuyPlanetButton buyButton;
    public TextMesh price;

	// Use this for initialization
	void Awake () {
        huntButton = GetComponentInChildren<huntWildAliensButton>().gameObject;
        AliensOnPlanetStatus.SetActive(true);
        aliens = AliensOnPlanetStatus.GetComponentsInChildren<AlienState>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Activate()
    {
        hunted = false;
        base.Activate();
    }


    public override void Deactivate()
    {
        var huntMiniGame = transform.Find("huntMiniGame");
        if (huntMiniGame != null)
        {
            Destroy(huntMiniGame.gameObject);
        }
        base.Deactivate();
    }

    public override void UpdateContent()
    {
        if (hunted || !gameObject.activeInHierarchy) return;

        AliensOnPlanetStatus.SetActive(true);

        var thisPlane = Player.Instance.rocket.landedPlanet.GetComponent<WildPlanet>();

        huntButton.SetActive(!thisPlane.isYours);
        buyButton.gameObject.SetActive(!thisPlane.isYours);
        price.text = thisPlane.price.ToString();

        foreach (var alien in aliens)
        {
            alien.transform.Find("cage").gameObject.SetActive(false);
            int count = 0;
            if(thisPlane.randomSlaves != null)
            {
                var s = thisPlane.randomSlaves.Find(slave => slave.type == alien.type);
                if (s != null)
                {
                    count += s.count;
                }
            }

            //if(alien.type == thisPlane.alienType)
            //{
            //    count += thisPlane.population;
            //}
            alien.count.text = count.ToString();
        }
    }

    public void StartHunt()
    {
        AliensOnPlanetStatus.SetActive(false);
        hunted = true;
        huntButton.SetActive(false);
        //foreach (var alien in aliens)
        //{
        //    alien.transform.Find("cage").gameObject.SetActive(true);
        //}
    }
}
