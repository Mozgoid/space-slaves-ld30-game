﻿using UnityEngine;
using System.Collections;

public class MiniGameActor : MonoBehaviour
{

    public AlienType type;
    public GameObject cage;
    public bool isMovingRight;
    public float speed = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (cage && cage.activeInHierarchy) return; //already hunted

        transform.Translate(isMovingRight ? speed : -speed, 0, 0);
	}


    void OnMouseDown()
    {
        if (cage && cage.activeInHierarchy) return; //already hunted


        Debug.Log("you catch" + type.ToString());
        Player.Instance.AddAliens(type, 1);
        cage.SetActive(true);
    }
}
