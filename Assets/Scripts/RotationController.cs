﻿using UnityEngine;
using System.Collections;

public class RotationController : MonoBehaviour {

	public float rotationSpeed = 1.0f;
	
	void Start () 
	{
	
	}

	void FixedUpdate () 
	{
		transform.Rotate(Vector3.forward * rotationSpeed);

	}
}
