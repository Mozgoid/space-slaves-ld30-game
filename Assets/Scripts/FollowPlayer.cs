﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {
		var pos = Player.Instance.rocket.transform.position;
		transform.position = new Vector3 (pos.x, pos.y, transform.position.z);
	}
}
