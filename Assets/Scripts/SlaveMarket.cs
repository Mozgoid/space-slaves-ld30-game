﻿using UnityEngine;
using System.Collections.Generic;

public enum AlienType
{
	FirstAlien,

	Alien1 = FirstAlien,
	Alien2,
	ALien3,
	ALien4,
	ALien5,
	ALien6,
	AliensCount
}


public class Slave
{
	public AlienType type;
	public int count;
}


public class Quest
{
	public List<Slave> targets = new List<Slave>();
	public int reward = 10;
	public SlaveMarket client;

	//return true if required aliens present in inventory
	//and  add reward and sell slaves
	public bool TryComplete(List<Slave> inventory)
	{
		foreach (var yourSlave in inventory) {
			var questTarget = targets.Find(target => target.type == yourSlave.type);
			if(questTarget != null && questTarget.count > yourSlave.count) {
				Debug.Log("quest not complited");
				return false;
			}
		}

		int rewardSum = reward;

		foreach (var yourSlave in inventory) {
			var questTarget = targets.Find(target => target.type == yourSlave.type);
            if (questTarget != null)
            {
                rewardSum += client.Sell(yourSlave, questTarget.count);
		    }
		}

		Player.Instance.Money += rewardSum;
		Debug.Log ("quest complited");
		client.questGiven = false;
		GameManager.PlayerInventoryChanged (Player.Instance.slaves);
		return true;
	}
}

public class SlaveMarket : MonoBehaviour
{

	public List<Slave> slavesOnMarket = new List<Slave>();
	public bool questGiven;
	public AlienType nativeAliens;

	// Use this for initialization
	void Start () {
		slavesOnMarket = GenerateSlavesForMarket ();
	}


	List<Slave> GenerateSlavesForMarket()
	{
		var slaves = new List<Slave> ();
		for (AlienType alien = AlienType.FirstAlien; alien < AlienType.AliensCount; ++alien) {
			int minRange = alien == nativeAliens ? 15 : 5;
			int maxRange = alien == nativeAliens ? 25 : 10;
			slaves.Add(new Slave(){type = alien, count = Random.Range(minRange, maxRange)});
		}
		return slaves;
	}

	// Update is called once per frame
	void Update () {
	
	}

	//generates new quest
	public Quest GetQuest()
	{
		Debug.Log ("generating quest");
		questGiven = true;

        var rand = new System.Random();

        var randomSlaves = new List<Slave>();

        while (randomSlaves.Count < 3)
	    {
            var t = (AlienType)rand.Next((int)AlienType.FirstAlien, (int)AlienType.AliensCount);
	        while (randomSlaves.Find(s => s.type == t) != null)
	        {
                t = (AlienType)rand.Next((int)AlienType.FirstAlien, (int)AlienType.AliensCount);
	        }

            randomSlaves.Add(new Slave() { type = t, count = rand.Next(5, 10) });
	    }

		return new Quest (){reward = 150, client = this,targets = randomSlaves};
	}


	//return price of alien slaves based on their current amount on market
	public int Evaluate(AlienType type, int count)
	{
		float priceForOne = type == nativeAliens ? 5 : 10;
		//lower price if too many slaves of this type on market
		if (slavesOnMarket.Find (slave => slave.type == type).count > 15) {
				priceForOne *= 0.7f;
		}
		return (int)(priceForOne * count);
	}


	//add slaves to market, decrease their amount on player inventory and get reward
	public int Sell(Slave slave, int amountForTrade)
	{
		if (slave.count < amountForTrade) {
			Debug.LogError ("you don't have so many slaves");
			return 0;
		}

		int money = Evaluate (slave.type, amountForTrade);
		slave.count -= amountForTrade;

		var s = slavesOnMarket.Find(marketSlave => marketSlave.type == slave.type);

		if (s == null) { //no slave on market
			s = new Slave (){type = slave.type, count = amountForTrade};
			slavesOnMarket.Add (s);
		} else {
			s.count += amountForTrade;
		}

		
		GameManager.PlayerInventoryChanged (Player.Instance.slaves);

		return money;
	}

	public int Buy(Slave slave, int amountForTrade)
	{		
		int money = Evaluate (slave.type, amountForTrade);
		if (Player.Instance.Money < money) {
			Debug.LogError("not enough money");
			return 0;
		}

		var s = slavesOnMarket.Find(marketSlave => marketSlave.type == slave.type);
		
		if (s == null || s.count < amountForTrade) { //no slave on market			
			Debug.LogError ("market don't have so many slaves");
			return 0;
		} else {
			s.count -= amountForTrade;
		}
		
		GameManager.PlayerInventoryChanged (Player.Instance.slaves);
		return money;
	}

}
