﻿using UnityEngine;
using System.Collections.Generic;

public class Player : SingletonMonoBehaviour<Player>
{
	public TextMesh moneyCount;
	public List<Slave> slaves;
	public Rocket rocket;

	private Quest _quest;

	public Quest quest{
		get { return _quest;}
		set {
				_quest = value;
				GameManager.NewQuestGotten (_quest);
		}
	}

	private int _money = 0;

	public int Money {
		get { return _money; }
		set {
				_money = value >= 0 ? value : 0;
				moneyCount.text = _money.ToString ();
		}
	}

	void Start()
	{
		slaves = new List<Slave> ();
		for (AlienType t = AlienType.FirstAlien; t < AlienType.AliensCount; ++t) {
				slaves.Add (new Slave (){type = t, count = 2});
		}
		Money = 15;
	}

	public void AddAliens(AlienType alienType, int count)
	{
		var s = slaves.Find (slave => slave.type == alienType);
		if (s == null) {
			s = new Slave(){type = alienType, count = 0};
			Player.Instance.slaves.Add(s);
		}
		s.count += count;

        GameManager.PlayerInventoryChanged(slaves);
	}
}
