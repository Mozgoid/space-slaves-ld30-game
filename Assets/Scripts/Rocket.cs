﻿using UnityEngine;
using System.Collections;

public class Rocket : SingletonMonoBehaviour<Rocket>
{
	public Planet destination;
	public float speed = 1;
	public float startSpeed = 140;
    public GameObject openPlanetButton;

    private float _landingTime = 0;
    //false for few seconds after landing
    public bool canMove { get { return (Time.timeSinceLevelLoad - _landingTime) > 0.5; } }



	private bool _isMoving;

	public bool isMoving {
		get { return _isMoving; }
		set { 
			_isMoving = value;
			flameSprite.SetActive (_isMoving);
			flameParticles.enableEmission = _isMoving;
		}
	}

	public bool canLand = false;

	public Planet landedPlanet;
	public Planet lastLandedPlanet;
	public GameObject flameSprite;
	public ParticleSystem flameParticles;

	private Vector3 prevPos ;

	void Start () {
		GameManager.Instance.Rockets.Add(this);
		Player.Instance.rocket = this;
	}

	void Update()
	{
		var distanceFromCenter = transform.position.magnitude;
		
		if( distanceFromCenter > 40)
		{				
			var d= ViewManager.Instance.deathView.GetComponent<DeathView>();
			d.PlayerDead(DeathView.Reason.OutOfSpace);
			ViewManager.Instance.ChangeView(ViewManager.Instance.deathView);
		}
	}

	public void ReturnRocketToLastPlanet()
	{	
		//var radius = lastLandedPlanet.GetComponent<CircleCollider2D> ().radius;
		//transform.position = lastLandedPlanet.transform.position + lastLandedPlanet.transform.up * radius * 1.4f;
		flameParticles.Clear ();
		Land (lastLandedPlanet);
	}

	void FixedUpdate ()
	{
		if(isMoving)
		{
			//rigidbody2D.AddForce(transform.up * speed);

			Vector2 forward = transform.position - prevPos;
			transform.up = forward.normalized;

			prevPos = transform.position;
		}
	}

	void OnMouseDown()
	{
		/*if ( isMoving )
			return;

		Debug.Log("mouse down");
		FlyAway ();*/
	}

	void OnCollisionEnter2D(Collision2D other)
	{

        if (other.gameObject.name == "sun")
	    {
            var d = ViewManager.Instance.deathView.GetComponent<DeathView>();
            d.PlayerDead(DeathView.Reason.Sun);
            ViewManager.Instance.ChangeView(ViewManager.Instance.deathView);
            return;
	    }

		if ( !isMoving )
			return;

		Debug.Log("onCollisionEnter");

		if( canLand && other.gameObject.GetComponent<Planet>() != null)
		{
			Land(other.gameObject.GetComponent<Planet>());
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if ( rigidbody2D.isKinematic )
			return;
		
		Debug.Log("onCollisionExit");	
		canLand = true;
	}

	public void Land(Planet planet)
	{
		var slaveMarket = planet.GetComponent<SlaveMarket> ();
		if (slaveMarket != null && Player.Instance.quest == null) {
			Player.Instance.quest = slaveMarket.GetQuest();
		}
		Debug.Log ("landing");
		isMoving = false;
		rigidbody2D.isKinematic = true;
		canLand = false;

        Vector2 vectorToRocket = (transform.position - planet.transform.position);
        transform.up = vectorToRocket.normalized;
        Vector3 v3 = vectorToRocket.normalized;
	    var scale = planet.transform.localScale.x;
	    var circleCollider = planet.GetComponent<CircleCollider2D>();
	    Vector3 center = circleCollider.center;
        transform.position = planet.transform.position + v3 * 0.95f * circleCollider.radius * scale;
  
		transform.parent = planet.transform;
	    transform.localPosition += center; 
		landedPlanet = planet;
		lastLandedPlanet = planet;
	    rigidbody2D.velocity = Vector3.zero;
	    _landingTime = Time.timeSinceLevelLoad;


        if (planet.gameObject.GetComponent<SlaveMarket>() != null
            || planet.gameObject.GetComponent<WildPlanet>() != null)
        {
            openPlanetButton.SetActive(true);
        }
	}



	public void FlyAway()
	{
		Debug.Log ("Flying away");
		isMoving = true;
		
		transform.parent = null;
		landedPlanet = null;
		rigidbody2D.isKinematic = false;

		rigidbody2D.AddForce(transform.up * startSpeed);
		prevPos = transform.position - transform.up;

        openPlanetButton.SetActive(false);
	}

}
