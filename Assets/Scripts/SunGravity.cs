﻿using UnityEngine;
using System.Collections;

public class SunGravity : MonoBehaviour {


	public float sunGravity = 1.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		foreach(var rocket in GameManager.Instance.Rockets)
		{
			var vectToSun = transform.position - rocket.transform.position;
			rocket.rigidbody2D.AddForce(vectToSun * sunGravity);

		}
	}
}
