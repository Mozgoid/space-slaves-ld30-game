﻿using UnityEngine;
using System.Collections;

public class CompleteQuestButton : MonoBehaviour {

	
	void OnMouseDown()
	{
		Debug.Log ("press complite quest button");
		var quest = Player.Instance.quest;
		var planet = Player.Instance.rocket.landedPlanet;

		if (quest != null
		    && planet != null 
		    && quest.client == planet.GetComponent<SlaveMarket> ()) 
		{
			if(quest.TryComplete(Player.Instance.slaves))
			{
                Player.Instance.quest = quest.client.GetQuest();
			}
		}
	}
}
