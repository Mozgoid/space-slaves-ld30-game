﻿using UnityEngine;
using System.Collections;

public class CountButton : MonoBehaviour
{
	public enum CountMode
	{
		Inc,
		Dec
	};
	public CountMode mode;
	public SellButton button;
	void OnMouseDown()
	{
		if ( mode == CountMode.Inc )
		{
			button.incSellCount(1);
		}
		else
		{
			button.incSellCount(-1);
		}
	}
}
