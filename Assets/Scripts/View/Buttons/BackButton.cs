﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour 
{
	public ViewController solarSystemView;

	void OnMouseDown()
	{
		Debug.Log("on back");
		if ( solarSystemView != null )
		{
			ViewManager.Instance.ChangeView(solarSystemView);
		}
	}
}
