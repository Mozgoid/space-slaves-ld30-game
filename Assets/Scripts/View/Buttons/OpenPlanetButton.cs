﻿using UnityEngine;
using System.Collections;

public class OpenPlanetButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        var planet = Player.Instance.rocket.landedPlanet;
        if (planet != null
            && ViewManager.Instance.currentView == ViewManager.Instance.solarSystemView)
        {
            if (planet.gameObject.GetComponent<SlaveMarket>() != null)
            {
                ViewManager.Instance.ChangeView(ViewManager.Instance.planetView);
            }
            else if (planet.gameObject.GetComponent<WildPlanet>() != null)
            {
                ViewManager.Instance.ChangeView(ViewManager.Instance.wildPlanetView);
            }
        }
    }
}
