﻿using UnityEngine;
using System.Collections;

public class SellButton : MonoBehaviour 
{
	public AlienState alien;
	public TextMesh sellCount;
	public int count;

	void Start()
	{
		sellCount.text = count.ToString();
	}

	void OnMouseDown()
	{
		var cage = GetComponentInParent<AlienCage> ();
		var market = GetComponentInParent<PlanetViewController> ().market;
		if (cage.mode == AlienCage.CageMode.Buy) {			
			Player.Instance.Money -= ( market.Buy(alien.slaveInfo, count) );
		} else {
			Player.Instance.Money += ( market.Sell(alien.slaveInfo, count) );

		}
		count = 0;
		UpdateContent();
		alien.UpdateContent();
	}

	public void incSellCount(int value)
	{
		if ( count + value >= 0 && count + value <= alien.slaveInfo.count )
		{
			count += value;
			UpdateContent();
		}
	}

	void UpdateContent()
	{
		sellCount.text = count.ToString();
	}
}
