﻿using UnityEngine;
using System.Collections;

public class AlienState : MonoBehaviour
{
	public TextMesh count;
	public AlienType type;
	public Slave slaveInfo;


	public void setSlave(Slave slave)
	{
		slaveInfo = slave;
		UpdateContent();
	}

	public void UpdateContent()
	{
		count.text = slaveInfo.count.ToString();

	}

}
