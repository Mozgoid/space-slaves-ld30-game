﻿using UnityEngine;
using System.Collections.Generic;

public class DeathView : MonoBehaviour {

	public enum Reason
	{
		OutOfSpace,
		Sun
	}

	public GameObject[] sunBackgrounds;
	public GameObject[] outOfSpaceBackgrounds;

	// Use this for initialization
	void Start () {
	
	}

	public void PlayerDead(Reason reason)
	{
		foreach (var b in sunBackgrounds)	b.SetActive (false);
		foreach (var b in outOfSpaceBackgrounds)	b.SetActive (false);

		switch (reason) {
			case Reason.OutOfSpace:
				outOfSpaceBackgrounds[Random.Range(0, outOfSpaceBackgrounds.Length)].SetActive(true);
				break;
			case Reason.Sun:			
				sunBackgrounds[Random.Range(0, sunBackgrounds.Length)].SetActive(true);
				break;
			default: Debug.LogError("unknown death reason"); return;
		}

		var removed = RemoveSomeSlavesFroPlayer ();

		Player.Instance.rocket.ReturnRocketToLastPlanet ();


	}

	List<Slave> RemoveSomeSlavesFroPlayer()
	{
		var removed = new List<Slave> ();
		foreach (var s in Player.Instance.slaves) {
			var amount = Random.Range (s.count / 5, s.count / 2);
			s.count -= amount;
			removed.Add (new Slave (){ type = s.type, count = amount});
		}

		return removed;
	}

}
