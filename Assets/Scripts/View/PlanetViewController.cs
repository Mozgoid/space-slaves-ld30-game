﻿using UnityEngine;
using System.Collections;

public class PlanetViewController : ViewController
{
	public AlienCage playerCage;
	public AlienCage marketCage;
	public QuestView questView;

	public SlaveMarket market;

	public override void UpdateContent() 
	{
		market = Player.Instance.rocket.lastLandedPlanet.GetComponent<SlaveMarket> ();
		playerCage.UpdateContent (Player.Instance.slaves);
		//marketCage.UpdateContent (market.slavesOnMarket);
		questView.UpdateView ();
	}

	
	// Use this for initialization
	void Awake () {
		GameManager.PlayerInventoryChanged += playerCage.UpdateContent;
	}
	
	void OnDestroy () {
		GameManager.PlayerInventoryChanged -= playerCage.UpdateContent;
	}

}
