﻿using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;



public class QuestView : MonoBehaviour {

	public AlienState[] questVisualizers;

    public Transform[] questPlaces;

	// Use this for initialization
	void Awake () {
		GameManager.NewQuestGotten += ConfigureWithQuest;
	}

	void OnDestroy () {
		GameManager.NewQuestGotten -= ConfigureWithQuest;
	}


	public void UpdateView()
	{
		ConfigureWithQuest (Player.Instance.quest);
	}

	void ConfigureWithQuest(Quest quest)
	{
		if (quest == null) {
			Debug.LogError ("no quest");
			return;
		}

	    int indexOfFreePlace = 0;

		foreach(var s in questVisualizers)
		{
			var alien = quest.targets.Find(slave => slave.type == s.type);
			if(alien == null || alien.count == 0)
			{
				s.gameObject.SetActive(false);
			}
			else
			{
			    if (indexOfFreePlace < questPlaces.Length)
                {
                    s.gameObject.SetActive(true);
                    s.setSlave(alien);
			        s.transform.position = questPlaces[indexOfFreePlace].position;
			        indexOfFreePlace++;
			    }
			    else
                {
                    s.gameObject.SetActive(false);
			    }
			}
		}
	}
}
