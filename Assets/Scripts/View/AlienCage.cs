﻿using UnityEngine;
using System.Collections.Generic;

public class AlienCage : MonoBehaviour {

	public CageMode mode;
	public AlienState[] states;

	public enum CageMode
	{
		Sell,
		Buy
	}

	public void UpdateContent(List<Slave> slaves)
	{
		foreach(Slave slave in slaves)
		{
			foreach(AlienState state in states)
			{
				if ( slave.type == state.type )
				{
					state.setSlave(slave);
				}
			}
		}
	}

}
