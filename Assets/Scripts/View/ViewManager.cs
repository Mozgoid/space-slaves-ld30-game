﻿using UnityEngine;
using System.Collections;

public class ViewManager : SingletonMonoBehaviour<ViewManager>	
{
	public ViewController currentView;
	public ViewController previousView;
	public Camera cam;


	public ViewController deathView;
	public ViewController solarSystemView;
	public ViewController planetView;
	public WildPlanetViewContoller wildPlanetView;

	void Start ()
	{
	
	}

	public void ChangeView(ViewController view)
	{
		if ( view != currentView )
		{
			if ( currentView != null )
			{
				currentView.Deactivate();
			}
            
			previousView = currentView;

			currentView = view;
			currentView.Activate();

            var rocket = Player.Instance.rocket;
		    if (currentView == solarSystemView)
		    {
                rocket.gameObject.SetActive(true);
		        cam.GetComponent<FollowObject>().obj = rocket.transform;
		    }
		    else
            {
                rocket.gameObject.SetActive(false);
                cam.GetComponent<FollowObject>().obj = view.transform;
		    }

		}
	}

}
