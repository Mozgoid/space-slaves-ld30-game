﻿using UnityEngine;
using System.Collections;

public class ViewController : MonoBehaviour 
{
	public bool alwaysActive;

    public virtual void Activate()
    {
        gameObject.SetActive(true);
		UpdateContent();
	}

    public virtual void Deactivate()
	{
		if(!alwaysActive)
			gameObject.SetActive(false);
	}

	public virtual void UpdateContent()
	{
	}
}
