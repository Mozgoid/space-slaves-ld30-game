﻿using UnityEngine;
using System.Collections;

public class FollowObject : MonoBehaviour
{

    public Transform obj;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {
        var pos = obj.position;
		transform.position = new Vector3 (pos.x, pos.y, transform.position.z);
	}
}
