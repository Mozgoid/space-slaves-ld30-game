﻿using UnityEngine;
using System.Collections;

public class RocketController : MonoBehaviour {

	public float turnForce = 10;
	public float breakForce = 5;
	public Rocket rocket;
	// Use this for initialization
	void Start () {
		rocket = GetComponent<Rocket> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		var v = Input.GetAxis ("Vertical");

        if (!rocket.isMoving && v > 0 && rocket.canMove)
        {
			rocket.FlyAway();
		} 
        else if (rocket.isMoving) 
        {
		    if (rigidbody2D.velocity.magnitude > 0.2)
		    {
	            var xy = Input.GetAxis ("Horizontal");
	            rigidbody2D.AddForce (xy * turnForce * transform.right);

                rigidbody2D.AddForce(v * breakForce * transform.up);
		    }
            else if (v > 0)
            {
                rigidbody2D.AddForce(v * breakForce * transform.up);
            }
		}
	}
}
